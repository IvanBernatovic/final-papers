<?php

namespace App\Http\Controllers\Mentor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function index()
    {

    }

    public function create()
    {
    	return view('mentor-area.tasks.create');
    }
}
