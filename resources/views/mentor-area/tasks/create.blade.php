@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create new task</div>

                <div class="panel-body">
                    {!! Form::open(['method' => 'POST', 'route' => 'tasks.store']) !!}
                        <div class="form-group">
                            <label>Name</label>
                            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
                        </div>

                        <div class="form-group">
                            <label>Name (eng)</label>
                            {!! Form::text('name_eng', null, ['class' => 'form-control', 'required']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
