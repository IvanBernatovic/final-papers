<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
        	[
        		'name' => 'Administrator',
    			'email' => 'admin@ferit.hr',
    			'password' => bcrypt('password'),
    			'role_id' => 1
        	],
        	[
        		'name' => 'Professor',
    			'email' => 'professor@ferit.hr',
    			'password' => bcrypt('password'),
    			'role_id' => 2
        	],
        	[
        		'name' => 'Student',
    			'email' => 'student@ferit.hr',
    			'password' => bcrypt('password'),
    			'role_id' => 3
        	]
        ];

        foreach ($users as $user) {
        	\App\User::create($user);
        }
    }
}
