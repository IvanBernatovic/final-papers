<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['Administrator', 'Mentor', 'Student'];


        foreach ($roles as $role) {
        	\App\Models\Role::create(['name' => $role]);
        }
    }
}
