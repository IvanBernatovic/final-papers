<?php

use Illuminate\Database\Seeder;

class CourseTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Stručni', 'Preddiplomski', 'Diplomski'];


        foreach ($types as $type) {
        	\App\Models\CourseType::create(['name' => $type]);
        }
    }
}
